class Api::V1::CandidatesController < ActionController::Base
    def index
        @candidates = Candidate.all 
        render json: @candidates
    end

    def get_candidate
        @candidate = Candidate.find(params[:candidate_id])
        render json: @candidate
    end

    
    def create

        @candidate = Candidate.create!(candidate_params)
        render json: @candidate
        
    end

    def delete
        @candidate = Candidate.find(params[:candidate_id])
        @candidate.delete
        render json: {success: true}
    end

    def update
        @candidate = Candidate.find(params[:candidate_id])
        @candidate.update!(candidate_update_params)
        render json: @candidate
    end

    private 
    
    def candidate_params
        params.require(:candidate).permit(:followers, :name, :dateofbirth, :email_id, :password)
    end

    def candidate_update_params
        params.require(:candidate).permit(:followers, :name, :dateofbirth, :email_id, :password)
    end
end