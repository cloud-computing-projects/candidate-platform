class Candidate < ApplicationRecord
    has_many :domains
    has_many :educations
    has_many :experiences
end
