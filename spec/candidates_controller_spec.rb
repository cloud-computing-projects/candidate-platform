require 'rails_helper'

RSpec.describe "Api::V1::CandidatesControllers", type: :request do
  describe "GET Candidates" do
    let!(:candidate){create(:candidate,followers: 10000,name: "Pragathi",dateofbirth: "09/12/2001",email_id: "pragathisai0912@gmail.com",password: "YEUchiki@1505")}
    let!(:candidate1){create(:candidate,followers: 10000,name: "Pragathi",dateofbirth: "09/12/2001",email_id: "pragathisai0912@gmail.com",password: "YEUchiki@1505")}
    context '(success)' do
        before { get '/api/v1/candidates/get_all_candidates'}
        it 'Returns list of candidates' do
            puts JSON.parse(response.body)
        end
    end
  end

  describe "GET Candidate" do
    let!(:candidate){create(:candidate,followers: 10000,name: "Pragathi",dateofbirth: "09/12/2001",email_id: "pragathisai0912@gmail.com",password: "YEUchiki@1505")}
    context '(success)' do
        before { get "/api/v1/candidates/#{candidate.id}/get_candidate"}
        it 'Returns candidate' do
            puts JSON.parse(response.body)
        end
    end
  end

  describe 'POST /api/v1/candidates/create_candidate' do

    it 'returns the device attributes' do

        post '/api/v1/candidates/create_candidate', params: {candidate: {followers: 45, name: "Abc", dateofbirth: "01/01/01", email_id: "abc123@example.com", password: "abcdef"}}
        expect(JSON.parse(response.body)['followers']).to eql(45)
        expect(JSON.parse(response.body)['name']).to eql("Abc")
        expect(JSON.parse(response.body)['dateofbirth']).to eql("01/01/01")
        expect(JSON.parse(response.body)['email_id']).to eql("abc123@example.com")
        expect(JSON.parse(response.body)['password']).to eql("abcdef")
        expect(response.status).to eql(200)
    end
  end  
  
  describe ' Delete Candidate ' do
    let!(:candidate){create(:candidate,followers: 10000,name: "Pragathi",dateofbirth: "09/12/2001",email_id: "pragathisai0912@gmail.com",password: "YEUchiki@1505")}
    context '(Success)' do
      before { delete "/api/v1/candidates/#{candidate.id}/delete_candidate"}
      it 'Returns success message' do
        expect(JSON.parse(response.body)['success']).to eq(true)
        expect(response.code).to eq("200")
      end
    end
  end

  describe ' Update Candidate ' do
    let!(:candidate){create(:candidate,followers: 10000,name: "Pragathi",dateofbirth: "09/12/2001",email_id: "pragathisai0912@gmail.com",password: "YEUchiki@1505")}
    context '(Success)' do
      let!(:valid_attributes){
        {
          candidate:{
            followers: 20000
          }
        }
      }
      before { put "/api/v1/candidates/#{candidate.id}/update_candidate", params: valid_attributes}
      it 'Returns success message' do
        expect(JSON.parse(response.body)['followers']).to eq(20000)
        expect(response.code).to eq("200")
      end
    end
  end

end