FactoryBot.define do 
    factory :candidate do
        followers { 10000 }
        email_id { "pragathisai0912@gmail.com" }
        password { "blah" }
        dateofbirth { "09/12/2001" }
        name { "Pragathi K V" }
    end
end