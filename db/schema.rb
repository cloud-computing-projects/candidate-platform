# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_02_04_073847) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "candidates", force: :cascade do |t|
    t.integer "followers"
    t.string "name"
    t.string "dateofbirth"
    t.string "email_id"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "domains", force: :cascade do |t|
    t.string "name"
    t.bigint "candidate_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["candidate_id"], name: "index_domains_on_candidate_id"
  end

  create_table "educations", force: :cascade do |t|
    t.string "institution_name"
    t.string "degree"
    t.string "field_of_study"
    t.string "start_date"
    t.string "end_date"
    t.string "grade"
    t.string "description"
    t.bigint "candidate_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["candidate_id"], name: "index_educations_on_candidate_id"
  end

  create_table "experiences", force: :cascade do |t|
    t.string "title"
    t.string "employment_type"
    t.string "company_name"
    t.string "location"
    t.string "location_type"
    t.string "start_date"
    t.string "end_date"
    t.boolean "is_present"
    t.string "industry"
    t.string "description"
    t.bigint "candidate_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["candidate_id"], name: "index_experiences_on_candidate_id"
  end

  add_foreign_key "domains", "candidates"
  add_foreign_key "educations", "candidates"
  add_foreign_key "experiences", "candidates"
end
