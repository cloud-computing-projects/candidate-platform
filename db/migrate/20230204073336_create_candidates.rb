class CreateCandidates < ActiveRecord::Migration[7.0]
  def change
    create_table :candidates do |t|
      t.integer :followers
      t.string :name
      t.string :dateofbirth
      t.string :email_id
      t.string :password

      t.timestamps
    end
  end
end
