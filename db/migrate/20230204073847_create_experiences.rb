class CreateExperiences < ActiveRecord::Migration[7.0]
  def change
    create_table :experiences do |t|
      t.string :title
      t.string :employment_type
      t.string :company_name
      t.string :location
      t.string :location_type
      t.string :start_date
      t.string :end_date
      t.boolean :is_present
      t.string :industry
      t.string :description
      t.references :candidate, null: false, foreign_key: true

      t.timestamps
    end
  end
end
