class CreateEducations < ActiveRecord::Migration[7.0]
  def change
    create_table :educations do |t|
      t.string :institution_name
      t.string :degree
      t.string :field_of_study
      t.string :start_date
      t.string :end_date
      t.string :grade
      t.string :description
      t.references :candidate, null: false, foreign_key: true

      t.timestamps
    end
  end
end
