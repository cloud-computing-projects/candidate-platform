# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
100.times do 
    Candidate.create(followers: Faker::Number.digit,name: Faker::Name.name,dateofbirth: Faker::Date.between(from: Date.today, to: Date.today + 10.days).strftime("%d/%m/%Y"),email_id: Faker::Internet.email,password: Faker::Internet.password)
end
