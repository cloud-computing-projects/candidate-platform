Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
 # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      get '/candidates/get_all_candidates' , :to => "candidates#index"
      scope '/candidates/:candidate_id', :as => "candidate" do
        get 'get_candidate', :to => "candidates#get_candidate"
      end
      post '/candidates/create_candidate', :to => "candidates#create"
      scope '/candidates/:candidate_id', :as => "candidate" do
        delete 'delete_candidate', :to => "candidates#delete"
      end
      scope '/candidates/:candidate_id', :as => "candidate" do
        put 'update_candidate', :to => "candidates#update"
      end
    end
  end
end
